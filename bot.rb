# encoding: UTF-8
require 'rubygems'
require 'bundler/setup'
require 'discordrb'
require 'yaml'

#$stdout.reopen("log.txt", "w")
# Read the 'login' file, use it to instantiate the bot, then clear the variables used.

sensitiveData = File.read("login").split
bot = Discordrb::Commands::CommandBot.new token: "#{sensitiveData[0]}", client_id: sensitiveData[1], prefix: '!'
sensitiveData = ""

# I use this to make sure the bot can see commands. Intentionally hardcoded.
bot.message(content: 'hey bot') do |event|
	break unless event.user.id == 213647968878854145
	event.respond 'yep'
end

# I use this just in case I need to stop the bot and I don't have shell access. Intentionally hardcoded.
bot.command(:stop) do |event|
	break unless event.user.id == 213647968878854145
	puts("User #{event.user.username} has shut down the bot at #{Time.now}.")
	bot.stop
end

# Disable a command.
bot.command(:disable) do |event, commandToDisable|
	break unless permission_level("admins", event.user, event.user.server)
	serverConfig = YAML.load_file("configs/#{event.user.server.id}.yaml") # Load the current server's config file.
		for index in 0 ... serverConfig['disabled-commands'].size
			if serverConfig['disabled-commands'][index] == commandToDisable
				event.respond("This command is already disabled.")
				return
			end
		end
	serverConfig['disabled-commands'] = serverConfig['disabled-commands'].append(commandToDisable)
	File.open("configs/#{event.user.server.id}.yaml", "w") do |h|
		h.write serverConfig.to_yaml
	end
	event.respond("Command #{commandToDisable} is disabled. Well, it might have been, I haven't actually checked.")
end

bot.command(:enable) do |event, commandToEnable|
	break unless permission_level("admins", event.user, event.user.server)
	serverConfig = YAML.load_file("configs/#{event.user.server.id}.yaml") # Load the current server's config file.
	if commandToEnable == "nil"
		event.respond("pls no")
		return
	end
		for index in 0 ... serverConfig['disabled-commands'].size
			if serverConfig['disabled-commands'][index] == commandToEnable
				serverConfig['disabled-commands'].delete(commandToEnable)
					File.open("configs/#{event.user.server.id}.yaml", "w") do |h|
						h.write serverConfig.to_yaml
					end
				event.respond("The command should work now!")
				return
			end
		end
	event.respond("Whatever command that was, it definitely wasn't disabled.")
end

# God bless kizuna.
bot.command(:kizuna) do |event|
	break unless permission_level("trusted-users", event.user, event.user.server) && command_disabled("kizuna", event.user.server) == false
	files = Dir.entries("resources/kizuna")
	event.send_file(File.open("resources/kizuna/#{files[rand(files.size)]}"), caption: "HAI DOMO VIRTUAL YOUTUBER KIZUNA AI DESUUUUU~") #Pick a random file from the files list, and send it.
end

# Ensure you give proper headpats to all catgirls.
bot.command(:pat, min_args: 1, usage: "Mention a single user") do |event, user|
	break unless permission_level("trusted-users", event.user, event.user.server) && command_disabled("pat", event.user.server) == false
	if event.bot.parse_mention(user)
		event.respond " #{event.user.display_name} is patting #{event.bot.parse_mention(user).username}~"
	else event.respond "That's not a valid user, silly!"
	end
end

# Figure out how cool any given user is in a discord server.
bot.command(:rate) do |event, user|
	break unless permission_level("trusted-users", event.user, event.user.server) && command_disabled("rate", event.user.server) == false
	if event.bot.parse_mention(user)
		rating = (event.bot.parse_mention(user).id.to_i % 9) + 1 # Uses the mentioned user's ID to make a consistent random number between 1 and 10, with a minimum of 1.
		event.respond "I'd rate #{event.bot.parse_mention(user).username} #{rating}/10."
	else event.respond "That's not a valid user, silly!"
	end
end

# Make a handy-dandy swastika out of any two emojis you like!
bot.command(:swastika, max_args: 2, usage: "First emoji makes the shape, second emoji is filler (defaults to :ok_hand:).") do |event, emoji1=":heart:", emoji2=":ok_hand:"|
	break unless permission_level("trusted-users", event.user, event.user.server) && command_disabled("swastika", event.user.server) == false
	event.respond "Heil desu~\n
	#{emoji1} #{emoji2} #{emoji2} #{emoji1} #{emoji1} #{emoji1} #{emoji1}\n
	#{emoji1} #{emoji2} #{emoji2} #{emoji1} #{emoji2} #{emoji2} #{emoji2}\n
	#{emoji1} #{emoji2} #{emoji2} #{emoji1} #{emoji2} #{emoji2} #{emoji2}\n
	#{emoji1} #{emoji1} #{emoji1} #{emoji1} #{emoji1} #{emoji1} #{emoji1}\n
	#{emoji2} #{emoji2} #{emoji2} #{emoji1} #{emoji2} #{emoji2} #{emoji1}\n
	#{emoji2} #{emoji2} #{emoji2} #{emoji1} #{emoji2} #{emoji2} #{emoji1}\n
	#{emoji1} #{emoji1} #{emoji1} #{emoji1} #{emoji2} #{emoji2} #{emoji1}"
end

# A little rock-paper-scissors game you can play with the bot.
bot.command(:rps, min_args: 1, max_args: 1, usage: "Name a single unit, namely 'rock', 'paper', or 'scissors'.") do |event, unit|
	break unless permission_level("trusted-users", event.user, event.user.server) && command_disabled("rps", event.user.server) == false
	choices = ["rock", "paper", "scissors"]
	if choices.include?(unit) 
		playerChoice = choices.index(unit)
	elsif unit == "gun"
		event.respond "Bang. You're dead. Fuck you."
		break
	else 
		event.respond "That's not a valid input, silly!"
		break
	end
	response = rand(3)
	resultTable = [[0, -1, 1], [1, 0, -1], [-1, 1, 0]]
	result = resultTable[playerChoice][response]
	if result == 1 then event.respond "I chose #{choices[response]}, so you win, #{event.user.display_name}. Play again?"
	elsif result == 0 then event.respond "I chose #{choices[response]}, so it's a draw, #{event.user.display_name}. Play again?"
	elsif result == -1 then event.respond "I chose #{choices[response]}, so I win, #{event.user.display_name}. Play again?"
	end
end

# Welcome to the cumzone.
bot.command(:cum, bucket: :memes, rate_limit_message:'quit ejaculating.') do |event|
	break unless permission_level("trusted-users", event.user, event.user.server) && command_disabled("cum", event.user.server) == false
	resourceDirectory = File.join(Dir.pwd,'resources/cum')
	arrayOfFiles = Dir.entries(resourceDirectory)
	chosenArrayIndex = rand(arrayOfFiles.size)
	bot.voice_connect(event.user.voice_channel)
	voice_bot = event.voice
	voice_bot.play_file("#{resourceDirectory}/#{arrayOfFiles[chosenArrayIndex]}")
	bot.voice_destroy(event.user.server)
end

# The bot says bruh.
bot.command(:bruh, bucket: :memes, rate_limit_message:'bruh a little less, please') do |event|
	break unless permission_level("trusted-users", event.user, event.user.server)  && command_disabled("bruh", event.user.server) == false
	resourceDirectory = File.join(Dir.pwd,'resources/memes')
	if rand(10) == 0
		fileToPlay = "#{resourceDirectory}/bruhlovania.mp3"
	else
		fileToPlay = "#{resourceDirectory}/bruh.mp3"
	end
	bot.voice_connect(event.user.voice_channel)
	voice_bot = event.voice
	voice_bot.play_file(fileToPlay)
	bot.voice_destroy(event.user.server)
end

# Forces the bot to leave the voice chat in case something went wrong.
bot.command(:leave) do |event|
	event.voice.stop_playing()
	bot.voice_destroy(event.user.server)
end

# Some people prefer /r, and some prefer /roll. This is the best of both worlds!
bot.command(:r, usage: "Example usage: !r 1d20, !r 3d6+3, !r 100d48+23") do |event, *input|
	break unless permission_level("trusted-users", event.user, event.user.server)  && command_disabled("roll", event.user.server) == false
	roll_dice(event, input)
end

bot.command(:roll, usage: "Example usage: !r 1d20, !r 3d6+3, !r 100d48+23") do |event, *input|
	break unless permission_level("trusted-users", event.user, event.user.server)  && command_disabled("roll", event.user.server) == false
	roll_dice(event, input)
end

# Returns true if user has the parsed role (or is of the specified ID), false if not.
def permission_level(testRoles, testUser, testServer)
	serverConfig = YAML.load_file("configs/#{testServer.id}.yaml") # Load the current server's config file.
	if testRoles == "admins" # If you're looking for admins, the function processes this and nothing else.
	puts "User #{testUser.name} used an admin-only command at #{Time.now}."
		for index in 0 ... serverConfig["admins"].size
			if testUser.id == serverConfig["admins"][index]
				return true
			end
		end
		return false
	else
		for index in 0 ... serverConfig[testRoles].size # Search the list of all possible trusted users.
			if testUser.role?(testServer.roles.find {|role| role.name == serverConfig[testRoles][index]})
				return true
			end
		end
	return false
	end
end

# Returns true if the command has been explicitly disabled in the server's config.
def command_disabled(commandName, testServer)
	serverConfig = YAML.load_file("configs/#{testServer.id}.yaml") # Load the current server's config file.
	for index in 0 ... serverConfig["disabled-commands"].size # Search the list of all possible trusted users.
		if commandName == serverConfig["disabled-commands"][index]
			return true
		end
	end
	return false
end

def roll_dice(event, input)
dice = Array.new
total = 0
input = input.join
    input = input.gsub(/[","]/,'') # A comma isn't expected input, so we remove any that have been accidentally entered.
    input = input.gsub(/["d"]/,',') # Remove the 'd' from input and replace it with a sample character ','.
    input = input.gsub(/["+"]/,',+')
    input = input.gsub(/[\-]/,',-')
  input = input.split(',')
      if input[0] == ""
	input[0] = 1
      end
      for i in 0..input[0].to_i-1
        dice[i] = 1 + rand(input[1].to_i)
	total = total + dice[i]
      end
    total = total + input[2].to_i
   event.respond "#{event.user.mention} has rolled a total of #{total}.\nRolls: #{dice}"
end

bot.run
