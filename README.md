# Kool Bot

My Discord bot. Very much a work in progress, I'm currently in the process of moving the code over to this repository in a workable state. 
Although this project is public, it's for my personal use and is thus built specifically to fulfill my needs. You might notice a lot of links in the bot's code to files not included in this repository, this is by design. You will probably not be able to run the bot without either changing or removing the links to these files, and I won't be providing them. However, that said, feel free to use this project as a reference tool to build your own bot.

# Installation

## Dependencies

* First up, install ruby-full: https://www.ruby-lang.org/en/documentation/installation/
* Then, install discordrb: https://github.com/discordrb/discordrb
    * If you're getting errors regarding building the native extension, make sure you have the build-essential package installed. For some reason on openSUSE the package is "devel_basis" so use the following: ``sudo zypper install -t pattern devel_basis``
    * 
### Optional Dependencies

For the voice commands to work, the current official version of discordrb isn't working. I've included a Gemfile to fix this temporarily, by installing a fork that's been patched. Install bundler and use that to install discordrb instead if you need to use the voice commands.

## Adding your bot to your Discord account
1. Go to Discord's developer portal and follow the prompts to make a *bot* and a *user* so that you have a client ID and client secret. https://discord.com/developers/applications
2. Go to the following URL, replacing CLIENTID with your bot's client ID: https://discordapp.com/oauth2/authorize?client_id=CLIENTID&scope=bot and add it to the server you wish. **NOTE: You need to be an admin of the server.**

## Configuration

* Create a 'login' file (without an extension) in the repository's root directory. Ensure it contains your bot's user token and client ID, in that order, on seperate lines. The bot always checks for this on startup so you are free to distribute your bot.rb at any time should you wish to make changes.
* Run bot.rb with ruby and the discordrb gem installed, and hope you don't get any errors.

# Credits

kool-bot relies heavily on Discordrb by meew0: https://github.com/discordrb/discordrb


# License

This project doesn't have an official license, but feel free to use it if it's somehow useful to you in any way.
